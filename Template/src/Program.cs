﻿using System;
using System.Threading.Tasks;
using HumbleProgrammer.Framework;
using HumbleProgrammer.Shelf.Controllers;
using HumbleProgrammer.Shelf.Repository;
using HumbleProgrammer.Shelf.Service;
using HumbleProgrammer.Shelf.View;
using MVC_Example.Shelf.View;

namespace MVC_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncMain(args).GetAwaiter().GetResult();
        }

        async static Task AsyncMain(string[] args)
        {
            //Manual Dependency Injection
            var db = new Database();
            // Construct repositories and apply dependency
            var bookRepository = new BookRepository(db);

            var bookView = new BookView();

            // Create services
            var bookService = new BookService(bookRepository);

            // Create Mappers
            var bookMapper = new BookMapper();

            // Controllers
            var bookController = new BookController(bookService, bookMapper, bookView);

            // Routing
            var router = new Router(new object[] { bookController});
            var (success, message) = await router.Route(args);
            if (!success)
            {
                Console.WriteLine(message);
            }
            else
            {
                Console.WriteLine(message);
            }
        }
    }
}