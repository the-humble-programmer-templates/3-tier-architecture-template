using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HumbleProgrammer.Framework;
using HumbleProgrammer.Shelf.Data;
using HumbleProgrammer.Shelf.Domain;

namespace HumbleProgrammer.Shelf.Repository
{
    public class BookRepository : IBookRepository
    {
        private readonly Database _database;
        private const string TableKey = "book";

        public BookRepository(Database database)
        {
            _database = database;
        }

        public async Task<Book> Create(string title)
        {
            var book = new Book(Guid.NewGuid(), title);
            var data = ToDataModel(book);
            await _database.Save(TableKey, data.Id.ToString(), data);
            return book;
        }

        public async Task<IEnumerable<Book>> List()
        {
            var keys = await _database.TableKeys(TableKey);
            var bookDatas = await Task.WhenAll(keys.Select(x => _database.Load<BookData>(TableKey, x)));
            return bookDatas.Select(ToDomainModel);
        }

        public BookData ToDataModel(Book book)
        {
            if (book == null) return null;
            return new BookData(book.Title, book.Id);
        }

        public Book ToDomainModel(BookData data)
        {
            if (data == null) return null;
            return new Book(data.Id, data.Title);
        }
    }
}