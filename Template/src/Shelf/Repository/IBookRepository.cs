using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HumbleProgrammer.Shelf.Domain;

namespace HumbleProgrammer.Shelf.Repository
{
    public interface IBookRepository
    {
        
        Task<Book> Create(string title);

        Task<IEnumerable<Book>> List();
    }
}