using System;

namespace HumbleProgrammer.Shelf.ViewModel
{
    public class BookViewModel
    {
        public BookViewModel(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; }
        public string Title { get; }
    }
}