using System.Collections.Generic;
using System.Threading.Tasks;
using HumbleProgrammer.Shelf.Domain;

namespace HumbleProgrammer.Shelf.Service
{
    public interface IBookService
    {
        Task<Book> Create(string title);
        Task<IEnumerable<Book>> List();
    }
}