using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HumbleProgrammer.Shelf.Domain;
using HumbleProgrammer.Shelf.Repository;

namespace HumbleProgrammer.Shelf.Service
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _repository;

        public BookService(IBookRepository repository)
        {
            _repository = repository;
        }
        
        public Task<Book> Create(string title)
        {
            return _repository.Create(title);
        }

        public Task<IEnumerable<Book>> List()
        {
            return _repository.List();
        }
    }
}