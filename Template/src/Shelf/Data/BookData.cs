using System;

namespace HumbleProgrammer.Shelf.Data
{
    public class BookData
    {
        public BookData(string title, Guid id)
        {
            Title = title;
            Id = id;
        }

        public BookData()
        {
        }

        public string Title { get; set; }
        public Guid Id { get; set; }
    }
}