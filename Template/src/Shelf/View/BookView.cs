using System.Collections.Generic;
using System.Linq;
using HumbleProgrammer.Shelf.ViewModel;
using Kirinnee.Helper;
using MVC_Example.Shelf.View;

namespace HumbleProgrammer.Shelf.View
{
    public class BookView : IBookView
    {
        public string Create(BookViewModel vm)
        {
            if (vm == null) return "Failed to create book.";
            return $"<{vm.Title}> book of ID: {vm.Id} is created";
        }

        public string List(IEnumerable<BookViewModel> vm)
        {
            if (vm == null || !vm.Any()) return "There are no books";
            return "Books:\n\t" + vm.Select(x => x.Title + ": " + x.Id).JoinBy("\n\t");
        }
    }
}