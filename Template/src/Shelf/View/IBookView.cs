using System.Collections.Generic;
using HumbleProgrammer.Shelf.ViewModel;

namespace MVC_Example.Shelf.View
{
    public interface IBookView
    {
        string Create(BookViewModel vm);
        string List(IEnumerable<BookViewModel> vm);
    }
}