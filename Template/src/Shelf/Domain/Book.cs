using System;

namespace HumbleProgrammer.Shelf.Domain
{
    public class Book
    {
        public Book(Guid id, string title)
        {
            Id = id;
            Title = title;      
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}