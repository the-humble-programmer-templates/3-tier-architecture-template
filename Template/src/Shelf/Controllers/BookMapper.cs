using HumbleProgrammer.Shelf.Domain;
using HumbleProgrammer.Shelf.ViewModel;

namespace HumbleProgrammer.Shelf.Controllers
{
    public class BookMapper
    {

        public BookViewModel ToViewModel(Book book)
        {
            if (book == null) return null;
            return new BookViewModel(book.Id, book.Title);
        }
    }
}