using System;
using System.Linq;
using System.Threading.Tasks;
using HumbleProgrammer.Framework;
using HumbleProgrammer.Shelf.Service;
using MVC_Example.Shelf.View;

namespace HumbleProgrammer.Shelf.Controllers
{
    [Controller("book")]
    public class BookController
    {
        private readonly IBookService _service;
        private readonly BookMapper _bookMapper;
        private readonly IBookView _view;

        public BookController(IBookService service, BookMapper bookMapper,  IBookView view)
        {
            _service = service;
            _bookMapper = bookMapper;
            _view = view;
        }

        [Endpoint("create {title}")]
        public async Task<string> Create(string title)
        {
            // Interact with domain layer via service
            var resp = await _service.Create(title);

            // Convert to view model
            var viewModel = _bookMapper.ToViewModel(resp);

            // View renders 
            return _view.Create(viewModel);
        }

        [Endpoint("list")]
        public async Task<string> List()
        {
            var all = await _service.List();

            // Convert from domain model to view model
            var viewModel = all.Select(x => _bookMapper.ToViewModel(x));

            // View renders 
            return _view.List(viewModel);
        }
    }
}